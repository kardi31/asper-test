<?php

return [
    Symfony\Bundle\FrameworkBundle\FrameworkBundle::class => ['all' => true],
    Kardi\ApiBundle\KardiApiBundle::class => ['all' => true],
];
