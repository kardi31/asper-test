# Steps done to complete test task

1. Created API client to connect to API defined in https://www.thesportsdb.com/api.php?ref=apilist.fun
2. Created unit tests for the client. Each function has 3 tests, 1 for positive response and 2 for both exceptions that could be thrown
3. Created library called kardi/api, which contains the API client, current version is 0.1
4. Created this test application, which is using above package

# Additional information
1. I did not create Docker image, which was required in the task. I did not work on Dockers yet and I don't know how to set them up. I could use some tutorial, but I'd rather not do the task at all, than to do the task with some poor quality.

2. For unit tests I created a sample test client which overrides default `getClient` function with mocked requests. This way we could test full request process, including method `doRequest` which contains important logic regarding handling exceptions. 

3. `thesportsdb` API is not great. When we pass the parameter, which the API doesn't have the data for, it returns a blank page. I added in my API library a quick check, if the response we receive is blank and throw an exception on such case. We should not return a positive response for the data that does not exist. 
